﻿using RabbitMQ.Client;
using RabbitMQ.Core;
using System;

namespace RabbitMQ_NewTask
{
    class Program
    {
        public static void Main()
        {
            var events = new Events();

            var factory = new ConnectionFactory() { HostName = "localhost" };
            using var connection = factory.CreateConnection();

            //Create 50 message on to the Queue for processing
            for (int i = 0; i < 10; i++)
            {
                events.AddToEventQueue(connection, "RabbitMQ_task_queue", i.ToString());
            }

            events.AddToEventQueue(connection, "RabbitMQ_task_queue", "finished");


            //Wait for a response
            using (var channel = connection.CreateModel())
            {
                events.ProcessEvent(connection, channel, "response");

                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }
        }

       
    }
}