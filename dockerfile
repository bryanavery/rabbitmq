FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /app

COPY /RabbitMQ.sln ./

COPY /RabbitMQ_NewTask ./RabbitMQ_NewTask
COPY /RabbitMQ_Worker ./RabbitMQ_Worker
COPY /RabbitMQ.Core ./RabbitMQ.Core
RUN dotnet restore

COPY . ./
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/core/runtime:3.1

WORKDIR /app
COPY --from=build-env /app/out .

ENTRYPOINT ["dotnet", "RabbitMQ_Worker.dll"]
