# EVENT BUS WORKING SAMPLE #

how to use an Event Bus using RabbitMQ using Work Queues that will be used to distribute time-consuming tasks among multiple workers.

Getting started with an Event Bus is fast and easy. This quick start guide uses RabbitMQ with .NET Core. RabbitMQ must be installed, instructions for installing RabbitMQ are included here.

The main idea behind Work Queues (aka Task Queues) is to avoid doing a resource-intensive task immediately and having to wait for it to complete. Instead, we schedule the task to be done later. We encapsulate a task as a message and send it to a queue. A worker process running in the background will pop the tasks and eventually execute the job. When you run many workers the tasks will be shared between them.

This concept is especially useful in web applications where it’s impossible to handle a complex task during a short HTTP request window.

The code is self-explanatory, the NewTask creates 10 tasks on the queue (RabbitMQ_task_queue) at a time and the Worker will retrieve the queued items, with a 1-second delay, then responsed back to another queue (response) to let the NewTask process know that the work has been completed – to simulate some work being done.

## Round-robin dispatching ##
One of the advantages of using a Task Queue is the ability to easily parallelise work. If we are building up a backlog of work, we can just add more workers and that way, scale easily.

Try to run two Worker instances at the same time. They will both get messages from the queue, you will notice that each worker picks new items of the queue. By default, RabbitMQ will send each message to the next consumer, in sequence. On average every consumer will get the same number of messages. This way of distributing messages is called round-robin. Try this out with three or more workers.

## Message Acknowledgement ##
Doing a task can take a few seconds. You may wonder what happens if one of the consumers starts a long task and dies with it only partly done. With our current code, once RabbitMQ delivers a message to the consumer it immediately marks it for deletion. In this case, if you kill a worker we will lose the message it was just processing. We’ll also lose all the messages that were dispatched to this particular worker but were not yet handled.

But we don’t want to lose any tasks. If a worker dies, we’d like the task to be delivered to another worker.

In order to make sure a message is never lost, RabbitMQ supports message acknowledgements. An ack(nowledgement) is sent back by the consumer to tell RabbitMQ that a particular message has been received, processed and that RabbitMQ is free to delete it.

If a consumer dies (its channel is closed, the connection is closed, or TCP connection is lost) without sending an ack, RabbitMQ will understand that a message wasn’t processed fully and will re-queue it. If there are other consumers online at the same time, it will then quickly redeliver it to another consumer. That way you can be sure that no message is lost, even if the workers occasionally die.

There aren’t any message timeouts; RabbitMQ will redeliver the message when the consumer dies. It’s fine even if processing a message takes a very, very long time.

## Docker build process ##

1. Navigate to RabbitMQ_Worker.
2. Build the docker file there using the command:
docker build -t <whatever name you want to call your image> .
i) note the . at the end. It should be included in the command.
ii) do not include the <>. example: docker build -t worker .
3. The above builds an image from the command. To execute a container from the image, use the following command:
docker container run -it --name <any name you want to give that container> <name you gave the image from the previous instruction>.
i) Example command: docker container run -it --name workercontainer worker