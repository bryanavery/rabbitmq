﻿using RabbitMQ.Client;
using RabbitMQ.Core;
using System;

namespace RabbitMQ_Worker
{
    class Program
    {
        public static void Main()
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                new Events().ProcessEventWithResponse(connection, channel, "RabbitMQ_task_queue"); 
                
                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }


        }

        
    }

}
