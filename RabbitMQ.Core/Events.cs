﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Threading;

namespace RabbitMQ.Core
{
    public  class Events
    {
        private HiPerfTimer regTimer = new HiPerfTimer();

        public Events()
        {
            regTimer.Start();
        }

        public  void AddToEventQueue(IConnection connection, string queue, string mess)
        {
            using (var channel = connection.CreateModel())
            {
                // We make the queue durable so as it remains even after the service restarts
                channel.QueueDeclare(queue: queue, durable: true, exclusive: false, autoDelete: false, arguments: null);

                var message = $"Hello World - {mess}";
                var body = Encoding.UTF8.GetBytes(message);

                var properties = channel.CreateBasicProperties();
                properties.Persistent = true; // This ensures that the message stays on the queue even if a process fails

                channel.BasicPublish(exchange: "", routingKey: queue, basicProperties: properties, body: body);
                Console.WriteLine(" [x] Sent {0}", message);
            }
        }

        public  void ProcessEvent(IConnection connection, IModel channel, string queue)
        {
            channel.QueueDeclare(queue: queue, durable: true, exclusive: false, autoDelete: false, arguments: null);

            channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            Console.WriteLine(" [*] Waiting for messages.");

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body;
                var message = Encoding.UTF8.GetString(body);
                Console.WriteLine(" [x] Received {0}", message);

                if (message.Contains("finished"))
                {
                    regTimer.Stop();
                    Console.WriteLine("Time taken: {0}s", regTimer.Duration);

                    Console.WriteLine($"***********************{DateTime.UtcNow}");
                }

                channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
            };
            channel.BasicConsume(queue: queue, autoAck: false, consumer: consumer);
        }

        public void ProcessEventWithResponse(IConnection connection, IModel channel, string queue)
        {
            channel.QueueDeclare(queue: queue, durable: true, exclusive: false, autoDelete: false, arguments: null);

            channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            Console.WriteLine(" [*] Waiting for messages.");

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body;
                var message = Encoding.UTF8.GetString(body);
                Console.WriteLine(" [x] Received {0}", message);

                Thread.Sleep(1000);

                channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                AddToEventQueue(connection, "response", message);

                Console.WriteLine(" [x] Response Sent");
            };
            channel.BasicConsume(queue: queue, autoAck: false, consumer: consumer);
        }
    }
}
